const proyectoControllers = require('../controllers/proyectoController');
const proyectoControllers = require('../controllers/sprintController');
const express = require('express');
const router = express.Router();


router.post('/', proyectoControllers.create);

router.post('/ver/:id/create', sprintController.create);

router.get('/:page?', proyectoControllers.index);

router.get('/ver/:id', proyectoControllers.show);

router.delete('/:id?', proyectoControllers.remove);

/*
router.put('/:id', proyectoControllers.);
*/
module.exports = router;
