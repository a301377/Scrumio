const proyectoControllers = require('../controllers/sprintController');
const express = require('express');
const router = express.Router();


router.post('/:id', sprintController.create);

router.get('/:page?', sprintController.index);

router.get('/ver/:id', sprintController.show);

router.delete('/:id?', sprintController.remove);

/*
router.put('/:id', );
*/
module.exports = router;
