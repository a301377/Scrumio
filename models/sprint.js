const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;

const sprintSchema = Schema({
  numero : Number
});

proyectoSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Sprint', sprintSchema);
