const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;

const proyectoSchema = Schema({
  nombre : String,
  descripcion : String,
  fechaSol : String,
  fechaArr : String
});

proyectoSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Proyecto', proyectoSchema);
